import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RatingComponent {
  // values from 0 to 10
  values: number[] = Array(11)
    .fill(0)
    .map((_, index) => index);

  currentRating: number;

  rate(value: number) {
    this.currentRating = value;
  }
}
